﻿using DAL;
using EN;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BOL
{
    public class ProfesorBOL
    {
        public ProfesorDAL Pdal = new ProfesorDAL();
        public CursoDAL Cdal = new CursoDAL();



        /// <summary>
        /// Guarda los profesores en el XML
        /// </summary>
        /// <param name="Profe"></param>
        public void GuardarProfesor(Profesor Profe)
        {

            if (String.IsNullOrEmpty(Profe.Nombre)
                || String.IsNullOrEmpty(Profe.Cedula)
                || String.IsNullOrEmpty(Profe.Telefono)
                || String.IsNullOrEmpty(Convert.ToString(Profe.Pin))
                || String.IsNullOrEmpty(Profe.Direccion))
            {
                throw new Exception("Datos personales requeridos");
            }

            if (String.IsNullOrEmpty(Profe.Nombre)
                || String.IsNullOrEmpty(Profe.Direccion)
                || String.IsNullOrEmpty(Profe.Telefono))
            {
                throw new Exception("Datos del profesor requeridos");
            }
            if (MismoTel(Profe.Telefono))
            {
                throw new Exception("Ese telefono ya está registrado en el sistema");
            }
            if (MismaCed(Profe.Cedula))
            {
                throw new Exception("Esa cédula ya está registrada en el sistema");
            }

            if (File.Exists("Profesores.xml"))
            {
                Pdal.CrearXML(Environment.CurrentDirectory + "Profesores.xml", "Profesores");
                if (Pdal.LeerProfesores().Count == 0)
                {
                    Profe.Id = 1;
                    Profe.Pin = 1000;
                    Profe.Trabajando = false;
                }
                else
                {
                    Profe.Id = Pdal.LeerProfesores().Last().Id + 1;
                    if (Pdal.LeerProfesores().Last().Pin < 9999)
                    {
                        Profe.Pin = Pdal.LeerProfesores().Last().Pin + 1;
                        Profe.Trabajando = false;
                    }
                    else
                    {
                        throw new Exception("No se pueden registrar más usuarios");
                    }

                }
                Pdal.AñadirProfesor(Profe);
            }
            

        }

        /// <summary>
        /// Revisa que la cedula insertada no esté registrada en el sistema
        /// </summary>
        /// <param name="Cedula"></param>
        /// <returns></returns>
        private bool MismaCed(string Cedula)
        {
            foreach (Profesor item in Pdal.LeerProfesores())
            {
                if (Cedula.Equals(Convert.ToString(item.Cedula)))
                {
                    return true;
                } 
            }
            return false;
        }

        /// <summary>
        /// Revisa que el telefono registrado no esté registrado en el sistema
        /// </summary>
        /// <param name="Telefono"></param>
        /// <returns></returns>
        private bool MismoTel(string Telefono)
        {
            foreach (Profesor item in Pdal.LeerProfesores())
            {
                if (Telefono.Equals(Convert.ToString(item.Telefono)))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Revisa que el pin insertado esté bien
        /// </summary>
        /// <param name="pin"></param>
        /// <returns></returns>
        private bool SeguridadPIN(string pin)
        {
            return pin.Length >= 4;

        }

        /// <summary>
        /// Carga los profesores del XML
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        public List<Profesor> VerProfesores(String filtro)
        {
            if (String.IsNullOrEmpty(filtro))
            {
                return Pdal.LeerProfesores();
            }
            else
            {
                List<Profesor> filtrados = new List<Profesor>();
                foreach (Profesor item in Pdal.LeerProfesores())
                {
                    if (item.Nombre.Contains(filtro)|| item.Cedula.Contains(filtro)|| item.Telefono.Contains(filtro)
                        || item.Direccion.Contains(filtro)|| Convert.ToString(item.Pin).Contains(filtro))
                    {
                        filtrados.Add(item);
                    }
                }
                return filtrados;
            }
            
        }

        /// <summary>
        /// Manda a llamar al dal para editar los profesores
        /// </summary>
        /// <param name="profe"></param>
        public void EditarProfesor(Profesor profe)
        {
            Pdal.EditarProfesor(profe);
        }
        /// <summary>
        /// Manda a llamar al dal para borrar los profesores
        /// </summary>
        /// <param name="profe"></param>
        public void BorrarProfesor(Profesor profe)
        {
            Pdal.BorrarProfesor(profe);
        }

        /// <summary>
        /// Revisa que el pin insertado en el panel sea correcto
        /// </summary>
        /// <param name="pin"></param>
        /// <returns></returns>
        public Profesor VerificarPin(object pin)
        {
            foreach (Profesor item in Pdal.LeerProfesores())
            {
                if (Convert.ToString(item.Pin).Equals(pin))
                {
                    return item;
                }
                
            }
            return null;
        }

        /// <summary>
        /// Inserta los cursos al profesor
        /// </summary>
        /// <param name="profe"></param>
        /// <returns></returns>
        public List<Curso> InsertarCursos(Profesor profe)
        {
            List<Curso> cursos = new List<Curso>();
            foreach (var item in Cdal.LeerCursos())
            {
                if (item.PinProfesor.Equals(profe.Pin))
                {
                    cursos.Add(item);
                }
            }
            profe.Cursos = cursos;
            return profe.Cursos;
        }
    }
}
