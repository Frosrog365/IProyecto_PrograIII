﻿using DAL;
using EN;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{

    public class HorarioBOL
    {
        public HorarioDAL Hdal = new HorarioDAL();

        /// <summary>
        /// Guarda horarios
        /// </summary>
        /// <param name="horario"></param>
        public void GuardarHorario(Horario horario)
        {
            if (File.Exists("Horarios.xml"))
            {
                Hdal.CrearXML(Environment.CurrentDirectory + "Horarios.xml", "Horarios");
                if (Hdal.LeerHorarios().Count == 0)
                {
                    horario.Id = 1;
                    horario.Nombrecurso = "Sin asignar";
                }
                else
                {
                    horario.Id = Hdal.LeerHorarios().Last().Id + 1;
                    horario.Nombrecurso = "Sin asignar";




                }
                Hdal.AñadirHorarios(horario);
            }
        }

        /// <summary>
        /// manda a llamar al dal para editar horarios
        /// </summary>
        /// <param name="horario"></param>
        public void EditarHorario(Horario horario)
        {
            Hdal.EditarHorarios(horario);
        }

        /// <summary>
        /// Carga los horarios
        /// </summary>
        /// <returns></returns>
        public List<Horario> CargarHorarios()
        {
            return Hdal.LeerHorarios();
        }

        /// <summary>
        /// Borra los horarios
        /// </summary>
        /// <param name="horario"></param>
        public void BorrarHorario(Horario horario)
        {
            Hdal.BorrarHorarios(horario);
        }
    }
}
