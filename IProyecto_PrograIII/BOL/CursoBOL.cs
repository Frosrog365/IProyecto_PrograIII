﻿using DAL;
using EN;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public class CursoBOL
    {
        public CursoDAL Cdal = new CursoDAL();
        public HorarioDAL Hdal = new HorarioDAL();

        /// <summary>
        /// Guardar Cursos
        /// </summary>
        /// <param name="curso"></param>
        public void GuardarCurso(Curso curso)
        {
            if (String.IsNullOrEmpty(curso.Nombre)
                || String.IsNullOrEmpty(curso.NumAula))
            {
                throw new Exception("Datos del curso requeridos");
            }


            if (MismaAula(curso))
            {
                throw new Exception("Esa aula ya está ocupada en ese lapso de tiempo");
            }


            if (File.Exists("Cursos.xml"))
            {
                Cdal.CrearXML(Environment.CurrentDirectory + "Cursos.xml", "Cursos");
                if (Cdal.LeerCursos().Count == 0)
                {
                    curso.Id = 1;
                    curso.PinProfesor = 0;
                }
                else
                {
                    curso.Id = Cdal.LeerCursos().Last().Id + 1;
                    curso.PinProfesor = 0;




                }
                Cdal.AñadirCursos(curso);
            }


        }

        /// <summary>
        /// Insertar cursos al xml
        /// </summary>
        /// <param name="curso"></param>
        /// <returns></returns>
        public Horario InsertarHorario(Curso curso)
        {
            Horario horario = new Horario();
            foreach (var item in Hdal.LeerHorarios())
            {
                if (item.Nombrecurso.Equals(curso.Nombre))
                {
                    horario = item;
                }
            }
            curso.horario = horario;
            return curso.horario;
        }

        public List<Curso> CargarCursos()
        {
            return Cdal.LeerCursos();
        }

        /// <summary>
        /// Revisa que no se esté usando la misma aula en el lapso de tiempo
        /// </summary>
        /// <param name="curso"></param>
        /// <returns></returns>
        public bool MismaAula(Curso curso)
        {
            foreach (Curso item in Cdal.LeerCursos())
            {
                if (curso.NumAula.Equals(Convert.ToString(item.NumAula))
                    && curso.FechaInicio.Equals(item.FechaInicio) && curso.FechaFin.Equals(item.FechaFin))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Borra cursos del xml
        /// </summary>
        /// <param name="curso"></param>
        public void BorrarCurso(Curso curso)
        {
            Cdal.BorrarCursos(curso);
        }

        /// <summary>
        /// Manda a llamar al dal para editar los cursos
        /// </summary>
        /// <param name="curso"></param>
        public void EditarCurso(Curso curso)
        {
            Cdal.EditarCursos(curso);
        }
    }
}
