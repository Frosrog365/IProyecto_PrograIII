﻿using DAL;
using EN;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public class RegistroAsistenciaBOL
    {
        RegistroDAL rdal = new RegistroDAL();
        List<RegistroAsistencias> Registros = new List<RegistroAsistencias>();


        /// <summary>
        /// Guardar Registro de Asistencias BOL
        /// </summary>
        /// <param name="Ra">Registro que se quiere guardar</param>
        public void GuardarRegistro(RegistroAsistencias Ra)
        {
            rdal.LeerRegistros();
            if (File.Exists("Registros.xml"))
            {
                if (String.IsNullOrEmpty(Ra.Justificacion))
                {
                    Ra.Justificacion = "Curso";
                }
                rdal.CrearXML(Environment.CurrentDirectory + "Registros.xml", "Registros");
                rdal.AñadirRegistro(Ra);

            }

            //public List<RegistroAsistencias> SacarRegistros()
            //{
            //    return Registros = rdal.SacarRegistros();
            //}
        }

        /// <summary>
        /// Manda a llamar al dal para cargar los registros
        /// </summary>
        /// <returns></returns>
        public List<RegistroAsistencias> CargarRegistros()
        {
            return rdal.LeerRegistros();
        }

        /// <summary>
        /// Filtra los registros dependiendo de la cédula
        /// </summary>
        /// <param name="cedula"></param>
        /// <returns></returns>
        public List<RegistroAsistencias> RegistroFiltrados(string cedula)
        {
            List<RegistroAsistencias> todos = CargarRegistros();
            List<RegistroAsistencias> filtrados = new List<RegistroAsistencias>();
            for (int i = 0; i < todos.Count; i++)

            {
                if (todos.ElementAt(i).Cedula.Equals(cedula))
                {
                    filtrados.Add(todos.ElementAt(i));
                }
            }
            return filtrados;
        }

        /// <summary>
        /// Verifica que el profesor no pueda hacer su entrada más de una vez al día
        /// </summary>
        /// <param name="v"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool verificarRegistro(DateTime v, Profesor p)
        {
            for (int i = 0; i < CargarRegistros().Count; i++)
            {
                if(v.ToShortDateString().Equals(CargarRegistros().ElementAt(i).Dia) && p.Nombre.Equals(CargarRegistros().ElementAt(i).Nombre))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
