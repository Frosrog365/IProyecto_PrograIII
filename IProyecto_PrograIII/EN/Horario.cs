﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EN
{
    public class Horario
    {
        public int Id { get; set; }
        public string dia { get; set; }
        public string HoraInicio { get; set; }
        public string HoraFinal { get; set; }
        public string Nombrecurso { get; set; }
    }
}
