﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EN
{
    public class RegistroAsistencias
    {
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string Dia { get; set; }
        public string HoraLlegada { get; set; }
        public string HoraSalida { get; set; }
        public string Justificacion { get; set; }
    }
}
