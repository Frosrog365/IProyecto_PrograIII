﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EN
{
    public class Curso
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string NumAula { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
        public int PinProfesor { get; set; }
        public Horario horario { get; set; }
    }
}
