﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EN
{
    public class Profesor
    {
        public int Id { get; set; }
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public int Pin { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public List<Curso> Cursos { get; set; }
        public bool Trabajando { get; set; }
        public int Tardias { get; set; }
        public int Ausencias { get; set; }
        public int SalidasAnticipadas { get; set; }



    }
}
