﻿using BOL;
using EN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIInicio
{
    public partial class RegistroMarcas : Form
    {

        RegistroAsistenciaBOL Rbol;
        ProfesorBOL Pbol;

        public RegistroMarcas()
        {
            InitializeComponent();
        }

        private void RegistroMarcas_Load(object sender, EventArgs e)
        {
            Rbol = new RegistroAsistenciaBOL();
            Pbol = new ProfesorBOL();
            
            List<string> profes = new List<string>();
            foreach (Profesor item in Pbol.VerProfesores(""))
            {
                profes.Add(item.Nombre + " - " + item.Cedula);
            }
            profes.Add("Ver todos");
            comboBox1.DataSource = profes;
            comboBox1.SelectedIndex = profes.Count-1;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (comboBox1.SelectedItem.ToString().Equals("Ver todos"))
            {
                dataGridView1.DataSource = Rbol.CargarRegistros();
            }
            else
            {
                List<RegistroAsistencias> filtrados = new List<RegistroAsistencias>();
                string cedula = comboBox1.SelectedItem.ToString().Split('-').ElementAt(1).Trim();
                filtrados = Rbol.RegistroFiltrados(cedula);

                dataGridView1.DataSource = filtrados;
                
                
            }
        }
    }
}
