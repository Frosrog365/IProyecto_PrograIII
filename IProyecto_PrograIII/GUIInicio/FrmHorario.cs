﻿using BOL;
using EN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIInicio
{
    public partial class FrmHorario : Form
    {
        Curso curso;
        Horario horario;
        HorarioBOL Hbol;

        public FrmHorario()
        {
            InitializeComponent();
            btnAsignar.Visible = false;
        }

        public FrmHorario(Curso curso)
        {
            InitializeComponent();
            this.curso = curso;
            btnAsignar.Visible = true;

        }

        private void FrmHorario_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int row = dvgProfesor.SelectedRows.Count > 0 ? dvgProfesor.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                horario = dvgProfesor.CurrentRow.DataBoundItem as Horario;
                DialogResult dialogResult = MessageBox.Show("Desea Eliminar?", "Eliminar Horario", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Hbol.EditarHorario(horario);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int row = dvgProfesor.SelectedRows.Count > 0 ? dvgProfesor.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                horario = dvgProfesor.CurrentRow.DataBoundItem as Horario;
                DialogResult dialogResult = MessageBox.Show("Desea Eliminar?", "Eliminar Horario", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Hbol.BorrarHorario(horario);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmRegistroHoriarios Frh = new FrmRegistroHoriarios();
            Frh.Show(this);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int row = dvgProfesor.SelectedRows.Count > 0 ? dvgProfesor.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                horario = dvgProfesor.CurrentRow.DataBoundItem as Horario;
                DialogResult dialogResult = MessageBox.Show(String.Format("Desea Asignar este horario a {0}?", curso.Nombre), "Asignar Horarios", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    horario.Nombrecurso = curso.Nombre;
                    Hbol.EditarHorario(horario);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Hbol = new HorarioBOL();
            dvgProfesor.DataSource = Hbol.CargarHorarios();
        }

        private void CargarDatos()
        {
            dvgProfesor.DataSource = Hbol.CargarHorarios();
        }
    }
}
