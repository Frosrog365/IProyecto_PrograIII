﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using BOL;
using EN;

namespace GUIInicio
{
    public partial class RegistroProfesor : Form
    {
        ProfesorBOL Pbol;
        Profesor Profe;
        int id;
        int pin;
        int Ausencias;
        int Tardias;

        

        public RegistroProfesor()
        {
            InitializeComponent();
            
        }
        public RegistroProfesor(Profesor Profe)
        {
            InitializeComponent();
            this.Profe = Profe;
            CargarDatos();
            id = Profe.Id;
            pin = Profe.Pin;
            Ausencias = Profe.Ausencias;
            Tardias = Profe.Tardias;
            btnGuardar.Text = "Editar";
            
            
        }

        private void CargarDatos()
        {
            txtCedula.Text = Profe.Cedula;
            txtNombre.Text = Profe.Nombre;
            
            txtDireccion.Text = Profe.Direccion;
            txtTelefono.Text = Profe.Telefono;



        }

        private void RegistroProfesor_Load(object sender, EventArgs e)
        {
            Pbol = new ProfesorBOL();
            Profe = new Profesor();
        }

        private void SoloNum(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnGuardar.Text.Equals("Guardar"))
                {
                    Profe.Cedula = txtCedula.Text.Trim();
                    Profe.Direccion = txtDireccion.Text.Trim();
                    Profe.Nombre = txtNombre.Text.Trim();
                    
                    Profe.Telefono = txtTelefono.Text.Trim();
                    
                    
                    Pbol.GuardarProfesor(Profe);
                    MessageBox.Show("Profesor Guardado con éxito");
                    Close();


                }
                else
                {
                    Profe.Cedula = txtCedula.Text.Trim();
                    Profe.Direccion = txtDireccion.Text.Trim();
                    Profe.Nombre = txtNombre.Text.Trim();
                    
                    Profe.Telefono = txtTelefono.Text.Trim();
                    Profe.Id = id;
                    Profe.Pin = pin;
                    Profe.Ausencias = Ausencias;
                    Profe.Tardias = Tardias;
                    Pbol.EditarProfesor(Profe);
                    MessageBox.Show("Profesor Editado con éxito");
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void limpiar()
        {
            txtNombre.Text = "";
            txtCedula.Text = "";
            txtDireccion.Text = "";
            
        }

        private void RegistroProfesor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
            }
        }
    }
}
