﻿using BOL;
using EN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIInicio
{
    public partial class AgregarModificarEliminarCursos : Form
    {
        CursoBOL Cbol;
        ProfesorBOL Pbol;
        Profesor profe;
        Curso curso = new Curso();

        public AgregarModificarEliminarCursos()
        {
            InitializeComponent();
            btnAsignar.Visible = false;
        }

        public AgregarModificarEliminarCursos(Profesor Profe)
        {
            InitializeComponent();
            this.profe = Profe;
            btnAsignar.Visible = true;
            Pbol = new ProfesorBOL();
            Pbol.InsertarCursos(profe);

        }

        private void dvgProfesor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            cargarDatos();
        }

        private void cargarDatos()
        {
            Cbol = new CursoBOL();
            dvgProfesor.DataSource = Cbol.CargarCursos();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            int row = dvgProfesor.SelectedRows.Count > 0 ? dvgProfesor.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                curso = dvgProfesor.CurrentRow.DataBoundItem as Curso;
                DialogResult dialogResult = MessageBox.Show("Desea Eliminar?", "Eliminar Cursos", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    Cbol.BorrarCurso(curso);
                }




            }
        }

        private void AgregarModificarEliminarCursos_Load(object sender, EventArgs e)
        {
            Cbol = new CursoBOL();
            dvgProfesor.DataSource = Cbol.CargarCursos();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RegistroCursos Rc = new RegistroCursos();
            Rc.Show(this);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int row = dvgProfesor.SelectedRows.Count > 0 ? dvgProfesor.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                curso = dvgProfesor.CurrentRow.DataBoundItem as Curso;
                DialogResult dialogResult = MessageBox.Show(String.Format("Desea Asignar este curso a {0}?", profe.Nombre), "Eliminar Cursos", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    curso.PinProfesor = profe.Pin;
                    Cbol.EditarCurso(curso);
                }
            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            int row = dvgProfesor.SelectedRows.Count > 0 ? dvgProfesor.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                curso = dvgProfesor.CurrentRow.DataBoundItem as Curso;
                FrmHorario frm = new FrmHorario(curso);
                frm.Show(this);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
