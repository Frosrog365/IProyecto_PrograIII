﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EN;

namespace GUIInicio
{
    public partial class Justificación : Form
    {
        private RegistroAsistencias rs;
        private int v;

        public Justificación()
        {
            InitializeComponent();
        }

        public Justificación(RegistroAsistencias rs)
        {
            InitializeComponent();
            this.rs = rs;
        }

        public Justificación(RegistroAsistencias rs, int v)
        {
            InitializeComponent();
            this.rs = rs;
            this.v = v;
            List<String> opciones = new List<string>();
            opciones.Add("Consulta");
            opciones.Add("Reunión");
            opciones.Add("Otro.");
            if (v == 1)

            {

                textBox1.Visible = false;

                comboBox1.DataSource = opciones;
            }
            else if (v == 2)
            {
                label1.Text = "Justificación por salida anticipada";
                comboBox1.Visible = false;
                textBox1.Location = comboBox1.Location;
            }
            else
            {
                opciones = new List<string>();
                opciones.Add("Curso y Consulta");
                opciones.Add("Curso y Reunión");
                opciones.Add("Otro.");
                label1.Text = "Salida Tardía";
                textBox1.Visible = false;
                comboBox1.DataSource = opciones;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Justificación_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (v == 1)
            {
                this.rs.Justificacion = comboBox1.SelectedItem.ToString();
            }
            else if (v == 2)
            {
                this.rs.Justificacion = textBox1.Text.Trim();
            }
            else
            {
                this.rs.Justificacion = comboBox1.SelectedItem.ToString();
            }
            Close();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
