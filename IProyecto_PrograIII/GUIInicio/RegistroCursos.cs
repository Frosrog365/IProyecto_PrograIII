﻿using BOL;
using EN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIInicio
{
    public partial class RegistroCursos : Form
    {
        Curso curso = new Curso();
        CursoBOL bol = new CursoBOL();
        

        public RegistroCursos()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void RegistroCursos_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "dd/MM/yyyy";

            dateTimePicker2.Format = DateTimePickerFormat.Custom;
            dateTimePicker2.CustomFormat = "dd/MM/yyyy";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                curso.FechaInicio = dateTimePicker1.Value.ToShortDateString();
                curso.FechaFin = dateTimePicker2.Value.ToShortDateString();
                curso.Nombre = txtNombre.Text.Trim();
                curso.NumAula = txtNumAula.Text.Trim();
                bol.GuardarCurso(curso);
                


            }catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //curso.FechaInicio = dateTimePicker1.Value.ToString();
        }
    }
}
