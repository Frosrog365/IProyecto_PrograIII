﻿using BOL;
using EN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIInicio
{
    public partial class ModificarEliminarProfesores : Form
    {
        ProfesorBOL bol;
        public Profesor Profe;
        public String filtro = "";
        public ModificarEliminarProfesores()



        {
            InitializeComponent();
        }

        private void ModificarEliminarProfesores_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
            }
        }
        public void CargarProfesores(String filtro)
        {
            dvgProfesor.DataSource = bol.VerProfesores(filtro);
        }

        private void ModificarEliminarProfesores_Load(object sender, EventArgs e)
        {
            bol = new ProfesorBOL();
            CargarProfesores(txtFiltro.Text.Trim());
        }

        private void dvgProfesor_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int row = dvgProfesor.SelectedRows.Count > 0 ? dvgProfesor.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                Profe = dvgProfesor.CurrentRow.DataBoundItem as Profesor;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int row = dvgProfesor.SelectedRows.Count > 0 ? dvgProfesor.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                Profe = dvgProfesor.CurrentRow.DataBoundItem as Profesor;
                Console.WriteLine(Profe.Id);
                RegistroProfesor frm = new RegistroProfesor(Profe);
                frm.Show(this);
                Hide();

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int row = dvgProfesor.SelectedRows.Count > 0 ? dvgProfesor.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                Profe = dvgProfesor.CurrentRow.DataBoundItem as Profesor;
                DialogResult dialogResult = MessageBox.Show("Desea Eliminar?", "Eliminar Profesores", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    bol.BorrarProfesor(Profe);
                }




            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            bol = new ProfesorBOL();
            CargarProfesores(txtFiltro.Text.Trim());
            filtro = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RegistroProfesor Rp = new RegistroProfesor();
            Rp.Show(this);
            Hide();
        }

        private void dvgProfesor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtFiltro_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        private void txtFiltro_KeyUp(object sender, KeyEventArgs e)
        {
            filtro = txtFiltro.Text.Trim();
            bol = new ProfesorBOL();
            CargarProfesores(filtro.Trim());
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            bol = new ProfesorBOL();
            CargarProfesores(txtFiltro.Text.Trim());
            filtro = "";
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            int row = dvgProfesor.SelectedRows.Count > 0 ? dvgProfesor.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                Profe = dvgProfesor.CurrentRow.DataBoundItem as Profesor;

                AgregarModificarEliminarCursos frm = new AgregarModificarEliminarCursos(Profe);
                frm.Show(this);

                }
                
        }
    }
}
