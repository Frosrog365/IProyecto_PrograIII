﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BOL;
using EN;

namespace GUIInicio
{
    public partial class Form1 : Form
    {
        ProfesorBOL Pbol;
        CursoBOL Cbol = new CursoBOL();
        String Pin;
        String dia = "";
        Profesor p;
        List<Profesor> profes;
        RegistroAsistenciaBOL rBOL;

        RegistroAsistencias Rs;

        public Form1()
        {
            InitializeComponent();



        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistroCursos Rp = new RegistroCursos();
            Rp.Show(this);
            Hide();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Pbol = new ProfesorBOL();
            Pin = "";
            profes = Pbol.VerProfesores("");
            InsertarTodo();
            sacarDia();
            Rs = new RegistroAsistencias();
            rBOL = new RegistroAsistenciaBOL();
            timer1.Start();


        }

        private void modificarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void modificarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        public void InsertarTodo()
        {
            foreach (Profesor item in profes)
            {
                Pbol.InsertarCursos(item);
                foreach (Curso v in item.Cursos)
                {
                    Cbol.InsertarHorario(v);
                }

            }
        }

        private void profesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificarEliminarProfesores frm = new ModificarEliminarProfesores();
            frm.Show(this);
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Pin))
            {
                Pin = "1";
            }
            else if (Pin.Length < 4)
            {
                Pin += 1;
            }
            label1.Text = Pin;


        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Pin))
            {
                Pin = "2";
            }
            else if (Pin.Length < 4)
            {
                Pin += 2;
            }
            label1.Text = Pin;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Pin))
            {
                Pin = "3";
            }
            else if (Pin.Length < 4)
            {
                Pin += 3;
            }
            label1.Text = Pin;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Pin))
            {
                Pin = "4";
            }
            else if (Pin.Length < 4)
            {
                Pin += 4;
            }
            label1.Text = Pin;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Pin))
            {
                Pin = "5";
            }
            else if (Pin.Length < 4)
            {
                Pin += 5;
            }
            label1.Text = Pin;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Pin))
            {
                Pin = "6";
            }
            else if (Pin.Length < 4)
            {
                Pin += 6;
            }
            label1.Text = Pin;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Pin))
            {
                Pin = "7";
            }
            else if (Pin.Length < 4)
            {
                Pin += 7;
            }
            label1.Text = Pin;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Pin))
            {
                Pin = "8";
            }
            else if (Pin.Length < 4)
            {
                Pin += 8;
            }
            label1.Text = Pin;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Pin))
            {
                Pin = "9";
            }
            else if (Pin.Length < 4)
            {
                Pin += 9;
            }
            label1.Text = Pin;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Pin))
            {
                Pin = "0";
            }
            else if (Pin.Length < 4)
            {
                Pin += 0;
            }
            label1.Text = Pin;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Pin))
                Pin = Pin.Substring(0, Pin.Length - 1);
            label1.Text = Pin;
        }

        private void button12_Click(object sender, EventArgs e)
        {

            if (label1.Text == "0000")
            {
                menuStrip1.Enabled = true;
                label1.Text = "";
            }
            else if (Pbol.VerificarPin(Pin) != null)
            {

                label1.Text = "____";

                p = Pbol.VerificarPin(Pin);
                p.Cursos = Pbol.InsertarCursos(p);

                foreach (Curso Curso in p.Cursos)
                {
                    Curso.horario = Cbol.InsertarHorario(Curso);
                }
                Pin = "";
                DateTime dt = DateTime.Now;

                DateTime curso1 = DateTime.Parse(p.Cursos.ElementAt(0).horario.HoraInicio);


                //dt.ToString("HH:mm"); // 07:00 // 24 hour clock // hour is always 2 digits
                //dt.ToString("hh:mm tt"); // 07:00 AM // 12 hour clock // hour is always 2 digits
                //dt.ToString("H:mm"); // 7:00 // 24 hour clock
                //dt.ToString("h:mm tt"); // 7:00 AM // 12 hour clock
                sacarDia();
                string diaCurso = p.Cursos.ElementAt(0).horario.dia;
               
                if (!rBOL.verificarRegistro(dt,p))
                {
                    if (DateTime.Parse(p.Cursos.ElementAt(0).horario.HoraFinal) > dt && p.Trabajando)
                    {
                        p.SalidasAnticipadas++;
                        Justificación frm = new Justificación(Rs, 2);
                        frm.ShowDialog(this);
                    }
                    if (dia.Equals(p.Cursos.ElementAt(0).horario.dia) && (dt - DateTime.Parse(p.Cursos.ElementAt(0).horario.HoraFinal)).TotalHours >= 1 && p.Trabajando)
                    {

                        Justificación frm = new Justificación(Rs, 3);
                        frm.ShowDialog(this);
                    }

                    if (dt.TimeOfDay > curso1.TimeOfDay && dia.Equals(p.Cursos.ElementAt(0).horario.dia))
                    {

                        Rs.Cedula = p.Cedula;
                        Rs.Nombre = p.Nombre;
                        Rs.Dia = dia;
                        if (!p.Trabajando)
                        {
                            Rs.HoraLlegada = dt.ToString("hh:mm tt");
                            p.Trabajando = true;
                            System.Media.SoundPlayer player = new System.Media.SoundPlayer("D:/Documentos/UTN/PrograIII/IProyecto/IProyecto_PrograIII/GUIInicio/Resources/AC.wav");
                            player.Play();
                        }
                        else
                        {
                            Rs.HoraSalida = dt.ToString("hh:mm tt");
                            Rs.Dia = dt.ToString("dd/MM/yyyy");
                            rBOL.GuardarRegistro(Rs);
                            p.Trabajando = false;
                            p.Tardias++;
                            if (p.SalidasAnticipadas != 0 && (p.Tardias % 2 == 0 || p.SalidasAnticipadas % 5 == 0))
                            {
                                p.Ausencias++;
                            }
                            Pbol.EditarProfesor(p);
                            System.Media.SoundPlayer player = new System.Media.SoundPlayer("D:/Documentos/UTN/PrograIII/IProyecto/IProyecto_PrograIII/GUIInicio/Resources/AC.wav");
                            player.Play();


                        }



                    }
                    else if ((curso1 - dt).TotalHours >= 1)
                    {
                        Rs.Cedula = p.Cedula;
                        Rs.Nombre = p.Nombre;
                        Rs.Dia = dia;
                        if (!p.Trabajando)
                        {
                            Justificación frm = new Justificación(Rs);
                            frm.Show(this);
                            Rs.HoraLlegada = dt.ToString("hh:mm tt");

                            p.Trabajando = true;
                            System.Media.SoundPlayer player = new System.Media.SoundPlayer("D:/Documentos/UTN/PrograIII/IProyecto/IProyecto_PrograIII/GUIInicio/Resources/AC.wav");
                            player.Play();
                        }
                        else
                        {
                            Rs.HoraSalida = dt.ToString("hh:mm tt");
                            Rs.Dia = dt.ToString("dd/MM/yyyy");
                            p.Trabajando = false;
                            rBOL.GuardarRegistro(Rs);
                            System.Media.SoundPlayer player = new System.Media.SoundPlayer("D:/Documentos/UTN/PrograIII/IProyecto/IProyecto_PrograIII/GUIInicio/Resources/AC.wav");
                            player.Play();


                        }
                    }
                    else if (!dia.Equals(p.Cursos.ElementAt(0).horario.dia))
                    {
                        Rs.Cedula = p.Cedula;
                        Rs.Nombre = p.Nombre;
                        Rs.Dia = dia;
                        if (!p.Trabajando)
                        {
                            Justificación frm = new Justificación(Rs, 1);
                            frm.ShowDialog(this);
                            Rs.HoraLlegada = dt.ToString("hh:mm tt");

                            p.Trabajando = true;
                            System.Media.SoundPlayer player = new System.Media.SoundPlayer("D:/Documentos/UTN/PrograIII/IProyecto/IProyecto_PrograIII/GUIInicio/Resources/AC.wav");
                            player.Play();
                        }
                        else
                        {
                            Rs.HoraSalida = dt.ToString("hh:mm tt");
                            Rs.Dia = dt.ToString("dd/MM/yyyy");
                            p.Trabajando = false;
                            rBOL.GuardarRegistro(Rs);
                            System.Media.SoundPlayer player = new System.Media.SoundPlayer("D:/Documentos/UTN/PrograIII/IProyecto/IProyecto_PrograIII/GUIInicio/Resources/AC.wav");
                            player.Play();


                        }
                    }
                    else if (dt < curso1 && dia.Equals(p.Cursos.ElementAt(0).horario.dia))
                    {
                        MessageBox.Show("Asistencia registrada");
                        Rs.Cedula = p.Cedula;
                        Rs.Nombre = p.Nombre;
                        Rs.Dia = dia;
                        if (!p.Trabajando)
                        {
                            Rs.HoraLlegada = dt.ToString("hh:mm tt");
                            p.Trabajando = true;
                            System.Media.SoundPlayer player = new System.Media.SoundPlayer("D:/Documentos/UTN/PrograIII/IProyecto/IProyecto_PrograIII/GUIInicio/Resources/AC.wav");
                            player.Play();
                        }
                        else
                        {
                            Rs.HoraSalida = dt.ToString("hh:mm tt");
                            p.Trabajando = false;
                            rBOL.GuardarRegistro(Rs);
                            System.Media.SoundPlayer player = new System.Media.SoundPlayer("D:/Documentos/UTN/PrograIII/IProyecto/IProyecto_PrograIII/GUIInicio/Resources/AC.wav");
                            player.Play();

                        }

                    }




                }
                else
                {
                    System.Media.SoundPlayer player = new System.Media.SoundPlayer("D:/Documentos/UTN/PrograIII/IProyecto/IProyecto_PrograIII/GUIInicio/Resources/op.wav");
                    player.Play();
                }
            }
            else
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer("D:/Documentos/UTN/PrograIII/IProyecto/IProyecto_PrograIII/GUIInicio/Resources/NR.wav");
                player.Play();
                label1.Text = "____";
                Pin = "";
            }

        }

        public void registrarAsistencias()
        {

        }

        public void sacarDia()
        {

            switch (DateTime.Now.DayOfWeek.ToString())
            {
                case "Monday":
                    dia = "Lunes";
                    break;
                case "Tuesday":
                    dia = "Martes";
                    break;
                case "Wednesday":
                    dia = "Miércoles";
                    break;
                case "Thursday":
                    dia = "Jueves";
                    break;
                case "Friday":
                    dia = "Viernes";
                    break;

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label2.Text = DateTime.Now.ToShortDateString() + "\n" + DateTime.Now.ToLongTimeString();

        }


        private void cursosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AgregarModificarEliminarCursos frm = new AgregarModificarEliminarCursos();
            frm.Show(this);
        }

        private void horariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmHorario frm = new FrmHorario();
            frm.Show(this);

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            registrarAsistencias();
        }

        private void registroDeMarcasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistroMarcas frm = new RegistroMarcas();
            frm.Show(this);
        }

        private void registroDeAusenciasYTardíasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistroAusenciasTardías frm = new RegistroAusenciasTardías();
            frm.Show(this);
        }

        private void profesorMásYMenosDestacadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<int> ausencias = new List<int>();
            string profe = "";
            foreach (Profesor item in Pbol.VerProfesores(""))
            {
                ausencias.Add(item.Ausencias);
            }
            int min = ausencias.Min();
            for (int i = 0; i < Pbol.VerProfesores("").Count; i++)
            {
                if (min == Pbol.VerProfesores("").ElementAt(i).Ausencias)
                {
                    profe = String.Format("El profesor más destacado es {0} con {1} ausencias", Pbol.VerProfesores("").ElementAt(i).Nombre, Pbol.VerProfesores("").ElementAt(i).Ausencias);
                    break;
                }
            }
            MessageBox.Show(profe);
        }

        private void profesorMásDestacadoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void profesorMásDestacadoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

        }

        private void profesorMenosDestacadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<int> ausencias = new List<int>();
            string profe = "";
            foreach (Profesor item in Pbol.VerProfesores(""))
            {
                ausencias.Add(item.Ausencias);
            }
            int max = ausencias.Max();
            for (int i = 0; i < Pbol.VerProfesores("").Count; i++)
            {
                if (max == Pbol.VerProfesores("").ElementAt(i).Ausencias)
                {
                    profe = String.Format("El profesor menos destacado es {0} con {1} ausencias", Pbol.VerProfesores("").ElementAt(i).Nombre, Pbol.VerProfesores("").ElementAt(i).Ausencias);
                    break;
                }
            }
            MessageBox.Show(profe);
        }
    }
}
