﻿using BOL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIInicio
{
    public partial class RegistroAusenciasTardías : Form
    {
        ProfesorBOL pBOL;
        public RegistroAusenciasTardías()
        {
            InitializeComponent();
        }

        private void RegistroAusenciasTardías_Load(object sender, EventArgs e)
        {
            pBOL = new ProfesorBOL();
            dataGridView1.DataSource = pBOL.VerProfesores("");
        }
    }
}
