﻿using BOL;
using EN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIInicio
{
    public partial class FrmRegistroHoriarios : Form
    {
        public Horario horario;
        public HorarioBOL Hbol;

        public FrmRegistroHoriarios()
        {
            InitializeComponent();
        }

        private void FrmRegistroHoriarios_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "hh:mm tt";

            dateTimePicker2.Format = DateTimePickerFormat.Custom;
            dateTimePicker2.CustomFormat = "hh:mm tt";
            horario = new Horario();
            Hbol = new HorarioBOL();
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            horario.dia = comboBox1.SelectedItem.ToString();
            horario.HoraInicio = dateTimePicker1.Text;
            horario.HoraFinal = dateTimePicker2.Text;
            Console.WriteLine(horario.dia + " " + horario.HoraInicio + " " + horario.HoraFinal);
            Hbol.GuardarHorario(horario);
        }
    }
}
