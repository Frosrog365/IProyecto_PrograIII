﻿using EN;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DAL
{


    public class RegistroDAL
    {
        public string rutaXML;
        public XmlDocument doc;
        List<RegistroAsistencias> ListRegistro = new List<RegistroAsistencias>();

        /// <summary>
        /// Crea el xml
        /// </summary>
        /// <param name="Ruta"></param>
        /// <param name="NodoRaiz"></param>
        public void CrearXML(string Ruta, string NodoRaiz)
        {
            this.rutaXML = Ruta;
            doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement element1 = doc.CreateElement(NodoRaiz);
            doc.AppendChild(element1);
            doc.Save(Ruta);
        }

        /// <summary>
        /// Añade el nodo al xml
        /// </summary>
        /// <param name="registro"></param>
        /// <returns></returns>
        public XmlNode AñadirRegistro(RegistroAsistencias registro)
        {
            doc = new XmlDocument();
            rutaXML = "Registros.xml";
            doc.Load(rutaXML);
            XmlNode Registro = CrearRegistro(registro);
            XmlNode nodoRaiz = doc.DocumentElement;
            nodoRaiz.InsertAfter(Registro, nodoRaiz.LastChild);
            doc.Save(rutaXML);

            return Registro;

        }

        /// <summary>
        /// Carga los registros
        /// </summary>
        /// <returns></returns>
        public List<RegistroAsistencias> LeerRegistros()
        {
            if (!File.Exists("Registros.xml"))
            {
                CrearXML("Registros.xml", "Registros");
            }
            doc = new XmlDocument();
            rutaXML = "Registros.xml";
            doc.Load(rutaXML);
            XmlNodeList Registros = doc.SelectNodes("Registros/Registro");
            XmlNode unRegistro;
            ListRegistro = new List<RegistroAsistencias>();

            for (int i = 0; i < Registros.Count; i++)
            {
                unRegistro = Registros.Item(i);
                RegistroAsistencias reg = new RegistroAsistencias
                {
                    Cedula = unRegistro.SelectSingleNode("Cedula").InnerText,
                    Nombre = unRegistro.SelectSingleNode("Nombre").InnerText,
                    HoraLlegada = unRegistro.SelectSingleNode("HoraLlegada").InnerText,
                    HoraSalida = unRegistro.SelectSingleNode("HoraSalida").InnerText,
                    Dia = unRegistro.SelectSingleNode("Dia").InnerText,
                    Justificacion = unRegistro.SelectSingleNode("Justificacion").InnerText
                };
                ListRegistro.Add(reg);


            }
            return ListRegistro;
        }

        /// <summary>
        /// Crea el nodo del registro
        /// </summary>
        /// <param name="Reg"></param>
        /// <returns></returns>
        private XmlNode CrearRegistro(RegistroAsistencias Reg)
        {

            XmlNode Registro = doc.CreateElement("Registro");


            

            XmlElement xNom = doc.CreateElement("Nombre");
            xNom.InnerText = Convert.ToString(Reg.Nombre);
            Registro.AppendChild(xNom);

            XmlElement xCed = doc.CreateElement("Cedula");
            xCed.InnerText = Convert.ToString(Reg.Cedula);
            Registro.AppendChild(xCed);

            XmlElement xHL = doc.CreateElement("HoraLlegada");
            xHL.InnerText = Convert.ToString(Reg.HoraLlegada);
            Registro.AppendChild(xHL);

            XmlElement xHS = doc.CreateElement("HoraSalida");
            xHS.InnerText = Convert.ToString(Reg.HoraSalida);
            Registro.AppendChild(xHS);

            XmlElement xDia = doc.CreateElement("Dia");
            xDia.InnerText = Convert.ToString(Reg.Dia);
            Registro.AppendChild(xDia);

            XmlElement xJust = doc.CreateElement("Justificacion");
            xJust.InnerText = Convert.ToString(Reg.Justificacion);
            Registro.AppendChild(xJust);

            return Registro;
        }
      

        
    }
}
