﻿using EN;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DAL
{
    public class ProfesorDAL
    {
        public string rutaXML;
        public XmlDocument doc;
        List<Profesor> ListProfesores = new List<Profesor>();

        /// <summary>
        /// Crea el xml
        /// </summary>
        /// <param name="Ruta"></param>
        /// <param name="NodoRaiz"></param>
        public void CrearXML(string Ruta, string NodoRaiz)
        {
            this.rutaXML = Ruta;
            doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement element1 = doc.CreateElement(NodoRaiz);
            doc.AppendChild(element1);
            doc.Save(Ruta);
        }

        /// <summary>
        /// Añade el nodo al xml
        /// </summary>
        /// <param name="Profe"></param>
        /// <returns></returns>
        public XmlNode AñadirProfesor(Profesor Profe)
        {
            doc = new XmlDocument();
            rutaXML = "Profesores.xml";
            doc.Load(rutaXML);
            XmlNode Profesor = CrearProfesor(Profe);
            XmlNode nodoRaiz = doc.DocumentElement;
            nodoRaiz.InsertAfter(Profesor, nodoRaiz.LastChild);
            doc.Save(rutaXML);

            return Profesor;

        }

        /// <summary>
        /// Carga los profesores
        /// </summary>
        /// <returns></returns>
        public List<Profesor> LeerProfesores()
        {
            if (!File.Exists("Profesores.xml"))
            {
                CrearXML("Profesores.xml", "Profesores");
            }
            doc = new XmlDocument();
            rutaXML = "Profesores.xml";
            doc.Load(rutaXML);
            XmlNodeList Profesores = doc.SelectNodes("Profesores/Profesor");
            XmlNode unProfesor;

            for (int i = 0; i < Profesores.Count; i++)
            {
                unProfesor = Profesores.Item(i);
                Profesor Profe = new Profesor
                {
                    Cedula = unProfesor.SelectSingleNode("Cedula").InnerText,
                    Nombre = unProfesor.SelectSingleNode("Nombre").InnerText,
                    Pin = Convert.ToInt32(unProfesor.SelectSingleNode("PIN").InnerText),
                    Direccion = unProfesor.SelectSingleNode("Direccion").InnerText,
                    Telefono = unProfesor.SelectSingleNode("Telefono").InnerText,
                    Id = Convert.ToInt32(unProfesor.SelectSingleNode("id").InnerText),
                    Tardias = Convert.ToInt32(unProfesor.SelectSingleNode("Tardias").InnerText),
                    Ausencias = Convert.ToInt32(unProfesor.SelectSingleNode("Ausencias").InnerText),
                    SalidasAnticipadas = Convert.ToInt32(unProfesor.SelectSingleNode("Salidas").InnerText)
                };
                if (unProfesor.SelectSingleNode("Trabajando").InnerText.Equals("False"))
                {
                    Profe.Trabajando = false;
                }
                else
                {
                    Profe.Trabajando = true;
                }
                ListProfesores.Add(Profe);


            }
            return ListProfesores;
        }

        /// <summary>
        /// Crea el nodo al xml
        /// </summary>
        /// <param name="Profe"></param>
        /// <returns></returns>
        private XmlNode CrearProfesor(Profesor Profe)
        {

            XmlNode Profesor = doc.CreateElement("Profesor");


            XmlElement xPin = doc.CreateElement("PIN");
            xPin.InnerText = Convert.ToString(Profe.Pin);
            Profesor.AppendChild(xPin);

            XmlElement xNom = doc.CreateElement("Nombre");
            xNom.InnerText = Convert.ToString(Profe.Nombre);
            Profesor.AppendChild(xNom);

            XmlElement xCed = doc.CreateElement("Cedula");
            xCed.InnerText = Convert.ToString(Profe.Cedula);
            Profesor.AppendChild(xCed);

            XmlElement xDir = doc.CreateElement("Direccion");
            xDir.InnerText = Convert.ToString(Profe.Direccion);
            Profesor.AppendChild(xDir);

            XmlElement xid = doc.CreateElement("id");
            xid.InnerText = Convert.ToString(Profe.Id);
            Profesor.AppendChild(xid);

            XmlElement xTel = doc.CreateElement("Telefono");
            xTel.InnerText = Convert.ToString(Profe.Telefono);
            Profesor.AppendChild(xTel);

            XmlElement xTrabajando = doc.CreateElement("Trabajando");
            xTrabajando.InnerText = Convert.ToString(Profe.Trabajando);
            Profesor.AppendChild(xTrabajando);

            XmlElement xTardias = doc.CreateElement("Tardias");
            xTardias.InnerText = Convert.ToString(Profe.Tardias);
            Profesor.AppendChild(xTardias);

            XmlElement xAusencias = doc.CreateElement("Ausencias");
            xAusencias.InnerText = Convert.ToString(Profe.Ausencias);
            Profesor.AppendChild(xAusencias);

            XmlElement xSalidas = doc.CreateElement("Salidas");
            xSalidas.InnerText = Convert.ToString(Profe.SalidasAnticipadas);
            Profesor.AppendChild(xSalidas);

            return Profesor;
        }

        /// <summary>
        /// Borra el profesor
        /// </summary>
        /// <param name="Profe"></param>
        public void BorrarProfesor(Profesor Profe)
        {
            doc = new XmlDocument();
            rutaXML = "Profesores.xml";
            doc.Load(rutaXML);
            XmlElement profesores = doc.DocumentElement;
            XmlNodeList listaProfes = doc.SelectNodes("Profesores/Profesor");
            foreach(XmlNode item in listaProfes)
            {
                if (item.SelectSingleNode("PIN").InnerText.Equals(Convert.ToString(Profe.Pin)))
                {
                    XmlNode seleccionado = item;
                    profesores.RemoveChild(seleccionado);
                }
            }
            doc.Save(rutaXML);

        }

        /// <summary>
        /// Edita el profesor
        /// </summary>
        /// <param name="Profe"></param>
        public void EditarProfesor(Profesor Profe)
        {
            doc = new XmlDocument();
            rutaXML = "Profesores.xml";
            doc.Load(rutaXML);
            XmlElement profesores = doc.DocumentElement;
            XmlNodeList listaProfres = doc.SelectNodes("Profesores/Profesor");

            XmlNode NuevoProfesor = CrearProfesor(Profe);
            foreach (XmlNode item in listaProfres)
            {
                if (item.SelectSingleNode("id").InnerText.Equals(Convert.ToString(Profe.Id)))
                {
                    XmlNode seleccionado = item;
                    profesores.ReplaceChild(NuevoProfesor, seleccionado);
                }
            }
            doc.Save(rutaXML);
            
        }
    }
}
