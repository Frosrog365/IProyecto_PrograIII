﻿using EN;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DAL
{
    public class CursoDAL
    {
        public string rutaXML;
        public XmlDocument doc;
        List<Curso> ListCursos = new List<Curso>();

        /// <summary>
        /// Crea el xml
        /// </summary>
        /// <param name="Ruta"></param>
        /// <param name="NodoRaiz"></param>
        public void CrearXML(string Ruta, string NodoRaiz)
        {
            this.rutaXML = Ruta;
            doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement element1 = doc.CreateElement(NodoRaiz);
            doc.AppendChild(element1);
            doc.Save(Ruta);
        }

        /// <summary>
        /// Añade un curso como nodo al XML
        /// </summary>
        /// <param name="curso"></param>
        /// <returns></returns>
        public XmlNode AñadirCursos(Curso curso)
        {
            doc = new XmlDocument();
            rutaXML = "Cursos.xml";
            doc.Load(rutaXML);
            XmlNode Curso = CrearCurso(curso);
            XmlNode nodoRaiz = doc.DocumentElement;
            nodoRaiz.InsertAfter(Curso, nodoRaiz.LastChild);
            doc.Save(rutaXML);

            return Curso;

        }

        /// <summary>
        /// Carga los cursos en el xml nuevo
        /// </summary>
        /// <returns></returns>
        public List<Curso> LeerCursos()
        {
            if (!File.Exists("Cursos.xml"))
            {
                CrearXML("Cursos.xml", "Cursos");
            }
            doc = new XmlDocument();
            rutaXML = "Cursos.xml";
            doc.Load(rutaXML);
            XmlNodeList Cursos = doc.SelectNodes("Cursos/Curso");
            XmlNode unCurso;

            for (int i = 0; i < Cursos.Count; i++)
            {
                unCurso = Cursos.Item(i);
                Curso curso = new Curso
                {
                    Nombre = unCurso.SelectSingleNode("Nombre").InnerText,
                    NumAula = unCurso.SelectSingleNode("NumeroAula").InnerText,
                    FechaInicio = unCurso.SelectSingleNode("FechaInicio").InnerText,
                    FechaFin = unCurso.SelectSingleNode("FechaFin").InnerText,
                    PinProfesor = Convert.ToInt32(unCurso.SelectSingleNode("PinProfesor").InnerText),
                    Id = Convert.ToInt32(unCurso.SelectSingleNode("Id").InnerText)
                };
                ListCursos.Add(curso);


            }
            return ListCursos;
        }

        /// <summary>
        /// Crea el nodo del curso
        /// </summary>
        /// <param name="curso"></param>
        /// <returns></returns>
        private XmlNode CrearCurso(Curso curso)
        {

            XmlNode Curso = doc.CreateElement("Curso");


            XmlElement xNom = doc.CreateElement("Nombre");
            xNom.InnerText = Convert.ToString(curso.Nombre);
            Curso.AppendChild(xNom);

            XmlElement xNumAula = doc.CreateElement("NumeroAula");
            xNumAula.InnerText = Convert.ToString(curso.NumAula);
            Curso.AppendChild(xNumAula);

            XmlElement xFechaInicio = doc.CreateElement("FechaInicio");
            xFechaInicio.InnerText = Convert.ToString(curso.FechaInicio);
            Curso.AppendChild(xFechaInicio);

            XmlElement xFechaFinal = doc.CreateElement("FechaFin");
            xFechaFinal.InnerText = Convert.ToString(curso.FechaFin);
            Curso.AppendChild(xFechaFinal);

            XmlElement xPinProfesor = doc.CreateElement("PinProfesor");
            xPinProfesor.InnerText = Convert.ToString(curso.PinProfesor);
            Curso.AppendChild(xPinProfesor);

            XmlElement xid = doc.CreateElement("Id");
            xid.InnerText = Convert.ToString(curso.Id);
            Curso.AppendChild(xid);

            return Curso;
        }

        /// <summary>
        /// Borra los cursos
        /// </summary>
        /// <param name="curso"></param>
        public void BorrarCursos(Curso curso)
        {
            doc = new XmlDocument();
            rutaXML = "Cursos.xml";
            doc.Load(rutaXML);
            XmlElement cursos = doc.DocumentElement;
            XmlNodeList listaCursos = doc.SelectNodes("Cursos/Curso");
            foreach (XmlNode item in listaCursos)
            {
                if (item.SelectSingleNode("Id").InnerText.Equals(Convert.ToString(curso.Id)))
                {
                    XmlNode seleccionado = item;
                    cursos.RemoveChild(seleccionado);
                }
            }
            doc.Save(rutaXML);

        }
        /// <summary>
        /// Edita los cursos
        /// </summary>
        /// <param name="curso"></param>
        public void EditarCursos(Curso curso)
        {
            doc = new XmlDocument();
            rutaXML = "Cursos.xml";
            doc.Load(rutaXML);
            XmlElement cursos = doc.DocumentElement;
            XmlNodeList listaCursos = doc.SelectNodes("Cursos/Curso");

            XmlNode NuevoCurso = CrearCurso(curso);
            foreach (XmlNode item in listaCursos)
            {
                if (item.SelectSingleNode("Id").InnerText.Equals(Convert.ToString(curso.Id)))
                {
                    XmlNode seleccionado = item;
                    cursos.ReplaceChild(NuevoCurso, seleccionado);
                }
            }
            doc.Save(rutaXML);

        }
    }
}
