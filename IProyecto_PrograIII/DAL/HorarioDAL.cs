﻿using EN;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DAL
{
    public class HorarioDAL
    {
        public string rutaXML;
        public XmlDocument doc;
        List<Horario> ListHorario = new List<Horario>();

        /// <summary>
        /// Crea el XML
        /// </summary>
        /// <param name="Ruta"></param>
        /// <param name="NodoRaiz"></param>
        public void CrearXML(string Ruta, string NodoRaiz)
        {
            this.rutaXML = Ruta;
            doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            XmlElement element1 = doc.CreateElement(NodoRaiz);
            doc.AppendChild(element1);
            doc.Save(Ruta);
        }

        /// <summary>
        /// Añade el nodo del horario al XML
        /// </summary>
        /// <param name="horario"></param>
        /// <returns></returns>
        public XmlNode AñadirHorarios(Horario horario)
        {
            doc = new XmlDocument();
            rutaXML = "Horarios.xml";
            doc.Load(rutaXML);
            XmlNode Horario = CrearHorario(horario);
            XmlNode nodoRaiz = doc.DocumentElement;
            nodoRaiz.InsertAfter(Horario, nodoRaiz.LastChild);
            doc.Save(rutaXML);

            return Horario;

        }

        /// <summary>
        /// Carga los nodos
        /// </summary>
        /// <returns></returns>
        public List<Horario> LeerHorarios()
        {
            if (!File.Exists("Horarios.xml"))
            {
                CrearXML("Horarios.xml", "Horarios");
            }
            doc = new XmlDocument();
            rutaXML = "Horarios.xml";
            doc.Load(rutaXML);
            XmlNodeList Horarios = doc.SelectNodes("Horarios/Horario");
            XmlNode unHorario;

            for (int i = 0; i < Horarios.Count; i++)
            {
                unHorario = Horarios.Item(i);
                Horario horario = new Horario
                {
                    dia = unHorario.SelectSingleNode("Dia").InnerText,
                    HoraInicio = unHorario.SelectSingleNode("HoraInicio").InnerText,
                    HoraFinal = unHorario.SelectSingleNode("HoraFinal").InnerText,
                    Nombrecurso = unHorario.SelectSingleNode("NombreCurso").InnerText,
                    Id = Convert.ToInt32(unHorario.SelectSingleNode("Id").InnerText)
                };
                ListHorario.Add(horario);


            }
            return ListHorario;
        }

        /// <summary>
        /// Crea el nodo del horario
        /// </summary>
        /// <param name="horario"></param>
        /// <returns></returns>
        private XmlNode CrearHorario(Horario horario)
        {

            XmlNode Horario = doc.CreateElement("Horario");


            XmlElement xId = doc.CreateElement("Id");
            xId.InnerText = Convert.ToString(horario.Id);
            Horario.AppendChild(xId);

            XmlElement xDia = doc.CreateElement("Dia");
            xDia.InnerText = Convert.ToString(horario.dia);
            Horario.AppendChild(xDia);

            XmlElement xHoraInicio = doc.CreateElement("HoraInicio");
            xHoraInicio.InnerText = Convert.ToString(horario.HoraInicio);
            Horario.AppendChild(xHoraInicio);

            XmlElement xHoraFinal = doc.CreateElement("HoraFinal");
            xHoraFinal.InnerText = Convert.ToString(horario.HoraFinal);
            Horario.AppendChild(xHoraFinal);

            XmlElement xNombreCurso = doc.CreateElement("NombreCurso");
            xNombreCurso.InnerText = Convert.ToString(horario.Nombrecurso);
            Horario.AppendChild(xNombreCurso);

            

            return Horario;
        }

        /// <summary>
        /// Borra los horarios
        /// </summary>
        /// <param name="horario"></param>
        public void BorrarHorarios(Horario horario)
        {
            doc = new XmlDocument();
            rutaXML = "Horarios.xml";
            doc.Load(rutaXML);
            XmlElement horarios = doc.DocumentElement;
            XmlNodeList listaHorarios = doc.SelectNodes("Horarios/Horario");
            foreach (XmlNode item in listaHorarios)
            {
                if (item.SelectSingleNode("Id").InnerText.Equals(Convert.ToString(horario.Id)))
                {
                    XmlNode seleccionado = item;
                    horarios.RemoveChild(seleccionado);
                }
            }
            doc.Save(rutaXML);

        }


        /// <summary>
        /// Edita los horarios
        /// </summary>
        /// <param name="horario"></param>
        public void EditarHorarios(Horario horario)
        {
            doc = new XmlDocument();
            rutaXML = "Horarios.xml";
            doc.Load(rutaXML);
            XmlElement horarios = doc.DocumentElement;
            XmlNodeList listaHorarios = doc.SelectNodes("Horarios/Horario");

            XmlNode NuevoHorario = CrearHorario(horario);
            foreach (XmlNode item in listaHorarios)
            {
                if (item.SelectSingleNode("Id").InnerText.Equals(Convert.ToString(horario.Id)))
                {
                    XmlNode seleccionado = item;
                    horarios.ReplaceChild(NuevoHorario, seleccionado);
                }
            }
            doc.Save(rutaXML);

        }
    }
}
